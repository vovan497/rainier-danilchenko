'use strict';

/**
 * App entry point.
 *
 * @module App
 */

/** Import initialized-by-default modules/libs */
import './components/Common';
import './components/PublicAPI';

/** Import page controllers */
import Home from './pages/Home';

import { currentPage } from './modules/dev/_helpers';

import {TweenMax, TimelineLite} from "gsap";

/**
 * Run appropriate scripts for each page.
 **/
switch (currentPage) {
  /** Home page */
  case 'home': new Home; break;

  /** No page found */
  default: console.warn('Undefined page');
}

$(document).ready(function() {

  // Add css animation for blue scroll down circle

  setTimeout(function() {
    $('.leave-logistic__scroll-circle_2').addClass('scrollDownCircle');
  }, 400);

  // $('.leave-logistic-scroll-to').click(function() {
  //   $('.leave-logistic__scroll-circle').removeClass('scrollDownCircle');
  //   $('.leave-logistic__scroll-circle_1').css({
  //     transition: 'transform 5s ease',
  //     transform: 'scale(200)'
  //   });
  // });

  $('.leave-logistic__scroll-to').click(function() {
    TweenMax.to(window, 1, {scrollTo:"#about-us"});
  });

  // Menu button

  $('.header__menu-btn').click(function() {
    $('.navigation_m').addClass('navigation_m-active');
  });

  $('.navigation-top_m__close-btn').click(function() {
    $('.navigation_m').removeClass('navigation_m-active');
  });
  
  // Screen 1 animations

  function firstScreenAnimations() {
    var tl = new TimelineLite();
    tl.fromTo( $('.header'), 1, {transform: 'translateY(-100px)'}, {transform: 'translateY(0)'})
      .fromTo( $('#svg_6'), 1, {transform: 'translateY(150px)'}, {transform: 'translateY(0)'}) // lake 
      .fromTo( $('.title-and-subtitle'), 1, 
        { opacity: 0,
          transform: 'translateY(-200px)'}, 
        { opacity: 1,
          transform: 'translateY(0)'}) // title, "scroll down" text
      .fromTo( $('#svg_7'), 1, {transform: 'translateX(340px)'}, {transform: 'translateY(0)'}) // trees 
      .to( [
        $('#svg_1532'),
        $('#g1766'),
        $('#svg_1'),
        $('#svg_812'),
        $('#svg_911')
      ], 1, {transform: 'translateX(0)'}) // mountains
      .to( [
        $('#svg_3'),
        $('#svg_2')
      ], 1, {transform: 'translate(0, 0)'}) // cloud, sun
      .fromTo( $('#svg_27'), 1, {transform: 'translateX(-1000px)'}, {transform: 'translateX(0)'}) // clouds_left
      .fromTo( $('#svg_910'), 1, {transform: 'translateX(-1000px)'}, {transform: 'translateX(0)'}) // bird
      .fromTo( $('#svg_909'), 1, {transform: 'translateX(-1000px)'}, {transform: 'translateX(0)'}) // bird
      .fromTo( $('#svg_1057'), 1, {transform: 'translateX(-1000px)'}, {transform: 'translateX(0)'}) // car
  };

  firstScreenAnimations();

  // Screen 2 animations

  $(window).scroll(function() {
    var scroll = $(this).scrollTop();
    if (scroll > 300) {
      var tl = new TimelineLite();
      tl.fromTo( $('.about-us-info__title'), 0.7, 
          { visibility: 'visible', opacity: 0, transform: 'translateY(-50px)' },
          { opacity: 1, transform: 'translateY(0)' })
        .fromTo( $('.about-us-info__txt'), 0.7, 
          { visibility: 'visible', opacity: 0, transform: 'translateY(-50px)' },
          { opacity: 1, transform: 'translateY(0)' })
        .fromTo( $('.about-us-info__btn'), 0.7, 
          { visibility: 'visible', opacity: 0, transform: 'translateY(-30px)' },
          { opacity: 1, transform: 'translateY(0)' })
      $(window).off('scroll');
    }
  });

  $('.about-us-pagination-btns__btn_america').click(function() {

    $(this).addClass('about-us-pagination-btns__btn_america-active');
    $('.about-us-pagination-btns__btn_canada').removeClass('about-us-pagination-btns__btn_canada-active');
    $('.about-us-pagination-btns__btn_mexico').removeClass('about-us-pagination-btns__btn_mexico-active');

    
    var tl = new TimelineLite();
    tl.to( [$('.about-us-slide-canada'), $('.about-us-slide-mexica')], 0.5, {visibility: 'hidden'} )
      .fromTo( $('.about-us-slide-ny'), 1, 
        { visibility: 'visible', opacity: 0, transform: 'translateX(900px)' },
        { opacity: 1, transform: 'translateX(0)' })
      .fromTo( $('.about-us-slide__ny-ground'), 1, 
        { transform: 'translateY(230px)' },
        { transform: 'translateY(0)', delay: 0.8 })
      .fromTo( $('.about-us-slide__ny-buildings'), 1, 
        { transform: 'translateY(340px)' },
        { transform: 'translateY(0)' })
      .fromTo( $('.about-us-slide__ny-clouds-sun'), 1, 
        { opacity: 0, transform: 'translateY(240px)' },
        { opacity: 1, transform: 'translateY(0)' })
      .fromTo( $('.about-us-slide__car'), 1, 
        { transform: 'translateX(-520px)' },
        { transform: 'translateX(0)' })

  });

  $('.about-us-pagination-btns__btn_canada').click(function() {

    $(this).addClass('about-us-pagination-btns__btn_canada-active');
    $('.about-us-pagination-btns__btn_america').removeClass('about-us-pagination-btns__btn_america-active');
    $('.about-us-pagination-btns__btn_mexico').removeClass('about-us-pagination-btns__btn_mexico-active');


    var tl = new TimelineLite();
    tl.to( [$('.about-us-slide-ny'), $('.about-us-slide-mexica')], 0.5, {visibility: 'hidden'} )
      .fromTo( $('.about-us-slide-canada'), 1, 
        { visibility: 'visible', opacity: 0, transform: 'translateX(900px)' },
        { opacity: 1, transform: 'translateX(0)' })
      .fromTo( $('.about-us-slide__canada-ground'), 1, 
        { transform: 'translateY(280px)' },
        { transform: 'translateY(0)', delay: 0.8 })
      .fromTo( $('.about-us-slide__canada-forest'), 1, 
        { transform: 'translateY(190px)' },
        { transform: 'translateY(0)' })
      .fromTo( $('.about-us-slide__canada-cloud-sun'), 1, 
        { opacity: 0, transform: 'translateY(160px)' },
        { opacity: 1, transform: 'translateY(0)' })
      .fromTo( $('.about-us-slide__car'), 1, 
        { transform: 'translateX(-520px)' },
        { transform: 'translateX(0)' })

  });

  $('.about-us-pagination-btns__btn_mexico').click(function() {

    $(this).addClass('about-us-pagination-btns__btn_mexico-active');
    $('.about-us-pagination-btns__btn_america').removeClass('about-us-pagination-btns__btn_america-active');
    $('.about-us-pagination-btns__btn_canada').removeClass('about-us-pagination-btns__btn_canada-active');


    var tl = new TimelineLite();
    tl.to( [$('.about-us-slide-ny'), $('.about-us-slide-canada')], 0.5, {visibility: 'hidden'} )
      .fromTo( $('.about-us-slide-mexica'), 1, 
        {
          visibility: 'visible',
          opacity: 0,
          transform: 'translateX(900px)'
        },
        {
          opacity: 1,
          transform: 'translateX(0)'
        })
      .fromTo( $('.about-us-slide__mexica-ground'), 1, 
        { transform: 'translateY(220px)' },
        { transform: 'translateY(0)', delay: 0.8 })
      .fromTo( $('.about-us-slide__mexica-buildings'), 1, 
        { transform: 'translateY(225px)' },
        { transform: 'translateY(0)' })
      .fromTo( $('.about-us-slide__mexica-sun-clouds'), 1, 
        { opacity: 0, transform: 'translateY(240px)' },
        { opacity: 1, transform: 'translateY(0)' })
      .fromTo( $('.about-us-slide__car'), 1, 
        { transform: 'translateX(-520px)' },
        { transform: 'translateX(0)' })

  });

});